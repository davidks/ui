import React, { Component } from "react";
import { Row, Col, CardColumns } from "react-bootstrap";
import { Card, Checkbox, FormControlLabel, DialogTitle, Button, Dialog, CardHeader, Icon, TextField, FormLabel, CardContent, Container, Divider, List, ListItem, InputAdornment, DialogContent, Chip, IconButton } from '@material-ui/core'
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import MaterialTable, { MTableToolbar } from "material-table";
import Axios from "axios";
import Moment from "moment";
import MomentUtils from "@date-io/moment";
import qs from 'qs';
import 'bootstrap/dist/css/bootstrap.css'
import { createMuiTheme } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import { ThemeProvider } from "@material-ui/styles";
import { lightBlue } from "@material-ui/core/colors";
import { Contacts, PeopleTwoTone, Phone, PhoneAndroid, Shop, ShoppingCart, Place, HomeOutlined, ArrowDownward } from "@material-ui/icons";


class DataTable extends Component {

    color = '#2FA4E7'

    theme = createMuiTheme({
        palette: {
            primary: lightBlue,
            secondary: green,
        },
        status: {
            danger: 'orange',
        },
    });



    tableBorderStyle = {
        borderColor: "rgb(224, 224, 224)",
        borderLeftStyle: "solid",
        borderWidth: 1
    }

    state = {
        userList: [],
        startActionDate: new Date(),
        endActionDate: new Date(),
        startOrderDate: new Date(),
        endOrderDate: new Date(),
        actionCheck: false,
        orderCheck: false,
        main_data: [],
        time_data: [],
        openModal: false,


    }

    constructor(props) {
        super(props);
        this.getDataTable = this.getDataTable.bind(this);
        this.onClose = this.onClose.bind(this)
        this.openModal = this.openModal.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.selectedUser = {

            accountNumber: React.createRef(),
            agent: React.createRef(),
            alamat: React.createRef(),
            namaToko: React.createRef(),
            le_code: React.createRef()

        }
    }



    editDataTable = () => {
        var data = {
            le_code: this.selectedUser.le_code,
            nama_toko: this.selectedUser.namaToko,
            account_number: this.selectedUser.accountNumber,
            alamat_toko: this.selectedUser.alamat
        }
        console.log(data)
        Axios.post("http://localhost:2019/master/updateSales", qs.stringify(data)).then((response) => {
            console.log(response)
            this.getDataTable()
        }).catch((err) => console.log(err))
    }

    getDataTable() {
        var data = {
            tgl_action_start: Moment(this.state.startActionDate).format("YYYY-MM-DD")
            , tgl_action_end: Moment(this.state.endActionDate).format("YYYY-MM-DD")
            , tgl_order_start: Moment(this.state.startOrderDate).format("YYYY-MM-DD")
            , tgl_order_end: Moment(this.state.endOrderDate).format("YYYY-MM-DD")
            , cek_action: this.state.actionCheck ? "true" : ""
            , cek_order: this.state.orderCheck ? "true" : ""
        }
        Axios.post("http://localhost:2019/master/laporan", qs.stringify(data)).then(response => {
            this.setState({ main_data: response.data.main, time_data: response.data.time })
        }).catch(err => console.log(err))
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.selectedUser[name] = value
        //  console.log(event.current.value)

    }

    date_picker = (label, onChange, value) => {
        return (<MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDatePicker className="row"
                autoOk
                variant="inline"
                format="DD/MM/YYYY"
                label={label}
                value={value}
                onChange={onChange}
            />
        </MuiPickersUtilsProvider>);
    }

    handleRowChange = (colum) => {
        this.selectedUser = {
            accountNumber: colum.account_number,
            agent: colum.users,
            alamat: colum.address_econ,
            namaToko: colum.nama_toko,
            le_code: colum.le_code
        }
        this.openModal()
    }



    onActionDateStartChange = startDate => this.setState({ startActionDate: new Date(startDate) })
    onActionDateEndChange = endDateAction => this.setState({ endActionDate: new Date(endDateAction) })
    onOrderDateStartChange = startDateOrder => this.setState({ startOrderDate: new Date(startDateOrder) })
    onOrderDateEndChange = endtDateOrder => this.setState({ endOrderDate: new Date(endtDateOrder) })
    onCheckActionChange = (event, checked) => this.setState({ actionCheck: checked })
    onCheckOrderChange = (event, checked) => this.setState({ orderCheck: checked })
    openModal = () => this.setState({ openModal: true })
    onClose = () => this.setState({ openModal: false })

    render() {

        return <ThemeProvider theme={this.theme}>
            <div className="container col-md-10">
                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"></link>
                <Card style={{ paddingLeft: 50, paddingRight: 40, paddingBottom: 20 }}>
                    {/* action */}
                    <Row >
                        <Col>
                            <FormControlLabel style={{ paddingTop: 10 }}
                                value="top"
                                control={<Checkbox color="primary" />}
                                label="Tanggal Action"
                                labelPlacement="end"
                                onChange={this.onCheckActionChange}
                            />
                        </Col>
                        <Col>

                            {this.date_picker("Start Date", this.onActionDateStartChange, this.state.startActionDate)}
                        </Col>
                        <Col>
                            {this.date_picker("End Date", this.onActionDateEndChange, this.state.endActionDate)}
                        </Col>


                    </Row>
                    {/* order */}
                    <Row>
                        <Col>
                            <FormControlLabel style={{ paddingTop: 10 }}
                                value="top"
                                control={<Checkbox color="primary" />}
                                label="Tanggal Order"
                                labelPlacement="end"
                                onChange={this.onCheckOrderChange}

                            />
                        </Col>
                        <Col>
                            {this.date_picker("Start Date", this.onOrderDateStartChange, this.state.startOrderDate)}
                        </Col>
                        <Col>
                            {this.date_picker("End Date", this.onOrderDateEndChange, this.state.endOrderDate)}
                        </Col>

                    </Row>

                    <Row>
                        <Button color="primary" variant="contained" onClick={this.getDataTable}> Proses</Button>
                    </Row>

                </Card>

                <Card style={{ marginTop: 10 }}>
                    <MaterialTable
                        components={
                            {
                                Toolbar: props => (
                                    <Row style={{ padding: 12 }}>
                                        <Col>
                                            <h3>Monitoring Data Sales</h3>
                                        </Col>
                                        <Col>
                                            <Button style={{ float: "right" }} >
                                                <ArrowDownward />
                                                Download Sebagai XLSX
                                        </Button>
                                        </Col>
                                    </Row>
                                )

                            }
                        }
                        options={{
                            search: false,
                            headerStyle: {
                                color: this.color,
                                borderColor: "white",
                                borderLeftStyle: "solid",
                                borderWidth: 1
                            }, exportButton: true

                        }}
                        title="Monitoring Data Sales"
                        style={{ borderRightWidth: 1 }}
                        columns={[
                            {
                                title: "AccountNumber"
                                , field: "account_number"
                                , headerStyle: { backgroundColor: this.color, color: "white" }
                            }, {
                                title: "Agent",
                                field: "users"
                                , cellStyle: this.tableBorderStyle
                                , headerStyle: { backgroundColor: this.color, color: "white" }
                            }, {
                                title: "Tanggal Order",
                                field: "tgl_order",
                                headerStyle: { backgroundColor: this.color, color: "white" },
                                cellStyle: this.tableBorderStyle
                            }, {
                                title: "Tanggal Action",
                                field: "date_last_action",
                                headerStyle: { backgroundColor: this.color, color: "white" },
                                cellStyle: this.tableBorderStyle
                            }, {
                                title: "Status",
                                render: (data) => {
                                    var statusInstall = data.status_aplikasi === 'C' ? "Install" : "Tidak Install"
                                    return (<div> {statusInstall} </div>)
                                }
                                , field: "status_aplikasi",
                                headerStyle: { backgroundColor: this.color, color: "white" },
                                cellStyle: this.tableBorderStyle
                            }, {
                                title: "Action",
                                render: (data) => { return <Button variant="contained" color="primary" onClick={() => { this.handleRowChange(data) }} > <Icon style={{ color: "white" }}>edit</Icon> Edit </Button> },
                                headerStyle: { backgroundColor: this.color, color: "white" },
                                cellStyle: this.tableBorderStyle,
                            }
                        ]}
                        data={this.state.main_data}
                    />
                </Card>
                <Card style={{ paddingTop: 10 }}>
                    <MaterialTable
                        title="Detail Hasil Penugasan"
                        options={
                            {
                                headerStyle: {
                                    background: this.color,
                                    borderColor: "white",
                                    color: "white",
                                    borderLeftStyle: "solid",
                                    borderWidth: 1
                                },search:false
                            }
                        }
                        columns={
                            [{
                                field: "account_number",
                                title: "Account Number"
                            }, {
                                title: "nama",
                                field: "slip_number"
                            }, {
                                title: "Hasil",
                                field: "result"
                            }, {
                                title: "Alasan No Install"
                                , field: "nominal"
                            }, {
                                title: "memo"
                                , field: "memo"
                            }, {
                                title: "tanggal input",
                                field: "datetime",
                                type:"datetime"
                                ,render:(data)=>(<div>{ Moment(data.datetime).format("DD-MM-YYYY")}</div>)
                            }]
                        }

                        data={this.state.time_data}
                    />
                </Card>
            </div>
            <Card style={{ padding: 0 }}>
                <Dialog
                    open={this.state.openModal}
                    onClose={this.onClose}
                    maxWidth="md"
                    scroll="paper"
                    fullWidth={true}
                >
                    <DialogTitle
                        style={{ background: this.color, color: "white" }} >Edit Data Sales</DialogTitle>


                    <DialogContent>

                        <CardContent >
                            <List>
                                <ListItem>

                                    <TextField
                                        disabled={true}
                                        fullWidth={true}
                                        label="Account Number"
                                        value={this.selectedUser.accountNumber}
                                        name="accountNumber"
                                        variant="outlined"
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <Contacts />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </ListItem>
                                <ListItem>
                                    <TextField
                                        fullWidth={true}
                                        label="Sobat"
                                        variant="outlined"
                                        value={this.selectedUser.agent}
                                        name="agent"
                                        disabled={true}
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <PeopleTwoTone />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </ListItem>
                                <ListItem>
                                    <TextField
                                        fullWidth={true}
                                        label="Nama Toko"
                                        variant="outlined"
                                        defaultValue={this.selectedUser.namaToko}
                                        name="namaToko"
                                        ref={this.selectedUser.namaToko}
                                        onChange={this.handleInputChange}
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <ShoppingCart />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </ListItem>
                                <ListItem>
                                    <TextField
                                        fullWidth={true}
                                        label="LE CODE"
                                        variant="outlined"
                                        name="le_code"
                                        defaultValue={this.selectedUser.le_code}
                                        ref={this.selectedUser.le_code}
                                        onChange={this.handleInputChange}
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <PhoneAndroid />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </ListItem>
                                <ListItem>
                                    <TextField
                                        fullWidth={true}
                                        label="Alamat Toko"
                                        name="alamat"
                                        variant="outlined"
                                        defaultValue={this.selectedUser.alamat}
                                        ref={this.selectedUser.alamat}
                                        onChange={this.handleInputChange}
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <Place />
                                                </InputAdornment>
                                            )
                                        }}
                                    />
                                </ListItem>
                                <ListItem>
                                    <Button style={{ marginRight: 10 }} variant="contained" onClick={this.editDataTable} color="primary">Save</Button>
                                    <Button variant="contained" style={{ background: "red" }} onClick=
                                        {this.onClose}>Cancel</Button>
                                </ListItem>
                            </List>
                        </CardContent>
                    </DialogContent>
                </Dialog>
            </Card>

        </ThemeProvider>



    }
}

export default DataTable